var AuthenticationController = require('./controllers/authentication'),  
    ListOfCountriesController = require('./controllers/country'),
    UserController = require('./controllers/user'),
    // TodoController = require('./controllers/todos'),  
    express = require('express'),
    passportService = require('../config/passport'),
    passport = require('passport');
 
var requireAuth = passport.authenticate('jwt', {session: false}),
    requireLogin = passport.authenticate('local', {session: false});
    
 
module.exports = function(app){
 
    var apiRoutes = express.Router(),
        authRoutes = express.Router();
        countryRoutes = express.Router();
        userRoutes = express.Router();
        // todoRoutes = express.Router();
 
    // Auth Routes
    apiRoutes.use('/auth', authRoutes, countryRoutes, userRoutes);
 
    authRoutes.post('/register', AuthenticationController.register);
    authRoutes.post('/login', requireLogin, AuthenticationController.login);
 
    authRoutes.get('/protected', requireAuth, function(req, res){
        res.send({ content: 'Success'});
    });
 
    // Country Routes
    countryRoutes.get('/country', ListOfCountriesController.country);

    // User Routes
    userRoutes.post('/updateuserlocation', UserController.UpdateUserLocation);
 
    // todoRoutes.get('/', requireAuth, AuthenticationController.roleAuthorization(['reader','creator','editor']), TodoController.getTodos);
    // todoRoutes.post('/', requireAuth, AuthenticationController.roleAuthorization(['creator','editor']), TodoController.createTodo);
    // todoRoutes.delete('/:todo_id', requireAuth, AuthenticationController.roleAuthorization(['editor']), TodoController.deleteTodo);
 
    // Set up routes
    app.use('/api', apiRoutes);
 
}
