var mongoose = require('mongoose');
 
var ListStates = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    code: {
        type: String,
        required: true
    }
});

var CountrySchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    states: [ListStates]
});

module.exports = mongoose.model('ce_countries', CountrySchema);
