var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var LocationCoordinates = new mongoose.Schema({
    latitude: {
        type: Number
    },
    longitude: {
        type: Number
    }
}, { _id : false });
 
var UserSchema = new mongoose.Schema({
 
    user_email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    user_password: {
        type: String,
        required: true
    },
    user_firstname: {
        type: String
    },
    user_lastname: {
        type: String,
    },
    user_role: {
        type: String,
        enum: ['admin', 'editor', 'reader'],
        default: 'reader'
    },
    user_location: [LocationCoordinates]
}, {
    timestamps: true
});

/* 
    permissions per role
        admin   = CRUD
        editor  = CRU
        reader  = R
*/
UserSchema.pre('save', function(next){
 
    var user = this;
    var SALT_FACTOR = 5;
 
    if(!user.isModified('user_password')){
        return next();
    } 
 
    bcrypt.genSalt(SALT_FACTOR, function(err, salt){
 
        if(err){
            return next(err);
        }
 
        bcrypt.hash(user.user_password, salt, null, function(err, hash){
 
            if(err){
                return next(err);
            }
 
            user.user_password = hash;
            next();
 
        });
 
    });
 
});
 
UserSchema.methods.comparePassword = function(passwordAttempt, cb){
 
    bcrypt.compare(passwordAttempt, this.user_password, function(err, isMatch){
 
        if(err){
            return cb(err);
        } else {
            cb(null, isMatch);
        }
    });
 
}
 
module.exports = mongoose.model('ce_users', UserSchema);
