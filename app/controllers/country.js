var Country = require('../models/country');

exports.country = function(req, res, next){
 
    Country.find().sort({name: 'asc'}).find({}, function(err, country){
        if(err){
          console.log(err);
        } else{
            res.json(country);
        }
    })

}