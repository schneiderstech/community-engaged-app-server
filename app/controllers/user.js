var User = require('../models/user');

exports.UpdateUserLocation = function(req, res, next){
 
    var user_email = req.body.user_email;
    var user_location = req.body.user_location;
 
    User.update({ user_email: user_email}, 
        { 
            $set: { 
                user_location: user_location
            }
        }, function(err, done) {
            if (err) {
                return res.json(err);
            }                    
            return  res.json(done);
        });


}