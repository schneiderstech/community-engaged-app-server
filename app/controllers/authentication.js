var jwt = require('jsonwebtoken');  
var User = require('../models/user');
var authConfig = require('../../config/auth');
var passport = require('passport');

function generateToken(user){
    return jwt.sign(user, authConfig.secret, {
        expiresIn: 604800
    });
}
 
function setUserInfo(request){
    return {
        _id: request._id,
        user_email: request.user_email,
        user_role: request.user_role
    };
}
 
exports.login = function(req, res, next){

    passport.authenticate('local', {session: false});

    var credentials = {
        user_email:req.body.user_email,
        user_password: req.body.user_password
    }

    var userInfo = setUserInfo(req.user);

    res.status(200).json({
        token: 'JWT ' + generateToken(userInfo),
        user: userInfo
    });
 
}
 
exports.register = function(req, res, next){
 
    var user_email = req.body.user_email;
    var user_password = req.body.user_password;
 
    if(!user_email){
        return res.status(422).send({error: 'You must enter an email address'});
    }
 
    if(!user_password){
        return res.status(422).send({error: 'You must enter a password'});
    }
 
    User.findOne({user_email: user_email}, function(err, existingUser){
 
        if(err){
            return next(err);
        }
 
        if(existingUser){
            return res.status(422).send({error: 'That email address is already in use'});
        }
 
        var user = new User({
            user_email: user_email,
            user_password: user_password
        });
 
        user.save(function(err, user){
 
            if(err){
                return next(err);
            }
 
            var userInfo = setUserInfo(user);
 
            res.status(201).json({
                token: 'JWT ' + generateToken(userInfo),
                user: userInfo
            })
 
        });
 
    });
 
}
 
exports.roleAuthorization = function(roles){
 
    return function(req, res, next){
 
        var user = req.user;
 
        User.findById(user._id, function(err, foundUser){
 
            if(err){
                res.status(422).json({error: 'No user found.'});
                return next(err);
            }
 
            if(roles.indexOf(foundUser.user_role) > -1){
                return next();
            }
 
            res.status(401).json({error: 'You are not authorized to view this content'});
            return next('Unauthorized');
 
        });
    } 
}
